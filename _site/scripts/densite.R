
#####################################################################################
# Calcul de la densité de commerces alimentaires par carreau INSPIRE ----
# Code Olivier Theureaux
#####################################################################################

# Packages

pacman::p_load(sf,tidyverse,tmap,geosphere, rgeos, sp, rgdal)

# Fichier source

stwd("~/OT/TRANSFOOD/DATA/")
list.files()e

## Chargements des carreaux INSPIRE

carreau <- st_read("data/Filosofi2015_carreaux_200m_metropole.gpkg")
carreau <- st_transform(carreau, crs = 4326)
carreau_light <- carreau %>% 
  select(IdINSPIRE,Depcom,geom)
rm(carreau)
# 2 289 070 lignes en France Métropolitaine
#st_write(carreau_light, "Filosofi2015_carreaux_200m_metropole_light.gpkg")

## Chargement de la couche commerces

commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg")
commerces <- st_transform(commerces, crs = 4326)

### Sélection des carreaux contenant des commerces

indices <- st_intersects(carreau_light, commerces)
carreaux_avec_points <- unlist(lapply(indices, any))
rm(indices)
carreaux_selectionnes <- carreau_light[carreaux_avec_points, ] 
rm(carreau_light)
# carreaux sélectionnés/filtrés = 147 034

# Calculer le nombre de points par carreau

carreaux_selectionnes$nb_commerces <- lengths(st_intersects(carreaux_selectionnes, commerces))

hist(carreaux_selectionnes$nb_commerces)
# densite <- st_intersects(carreaux_selectionnes$geom, st_centroid(commerces$geom))
# densite_com <- sapply(densite, length) 
# dim(data.frame(densite_com))

carreaux_selectionnes$densite <- 
  carreaux_selectionnes$nb_commerces / st_area(carreaux_selectionnes)

carreaux_selectionnes_final <- carreaux_selectionnes %>% 
  select(IdINSPIRE, nb_commerces, geom) %>% 
  as.data.frame()

getwd()
write.csv2(carreaux_selectionnes_final, "processed_data/carreaux_selectionnes_final.csv")



#####################################################################################
# Calcul de la densité de commerces alimentaires par carreau INSPIRE ----
# Possibilité de sélectionner une activité parmi 17 commerces alimentaires
# Code Olivier Theureaux
#####################################################################################

# Liste des activités
# 10.71B | Cuisson de produit de boulangerie
# 10.71C | Boulangerie et boulangerie-pâtisserie
# 10.71D | Pâtisserie
# 47.11A | Commerce de détail de produits surgelés
# 47.11B | Commerce d'alimentation générale
# 47.11C | Supérettes
# 47.11D | Supermarchés
# 47.11F | Hypermarchés
# 47.11E | Magasins multi-commerces
# 47.21Z | Commerce de détail de fruits et légumes en magasin spécialisé
# 47.22Z | Commerce de détail de viandes et de produits à base de viande en magasin spécialisé
# 47.23Z | Commerce de détail de poissons, crustacés et mollusques en magasin spécialisé
# 47.24Z | Commerce de détail de pain, pâtisserie et confiserie en magasin spécialisé
# 47.29Z | Autres commerces de détail alimentaires en magasin spécialisé
# 56.10A | Restauration traditionnelle
# 56.10B | Cafétérias et autres libres-services
# 56.10C | Restauration de type rapide

# Packages
pacman::p_load(sf,tidyverse,tmap,geosphere, rgeos, sp, rgdal)

##########################################
# Ici renseigner l'activité à étudier
commerces_selection <- '10.71C' # Boulangerie et boulangerie-pâtisserie
##########################################
# Fichier source
setwd("~/OT/TRANSFOOD/DATA/")
list.files()

## Chargements des carreaux INSPIRE
carreau <- st_read("data/Filosofi2015_carreaux_200m_metropole.gpkg")
carreau <- st_transform(carreau, crs = 4326)
carreau_light <- carreau %>% 
  select(IdINSPIRE,Depcom,geom)
rm(carreau)
# 2 289 070 lignes en France Métropolitaine
#st_write(carreau_light, "Filosofi2015_carreaux_200m_metropole_light.gpkg")

## Chargement de la couche commerces
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg")
commerces <- st_transform(commerces, crs = 4326)
commerces <- commerces%>% 
  filter(activitePrincipaleEtablissement == commerces_selection)

### Sélection des carreaux contenant des commerces
indices <- st_intersects(carreau_light, commerces)
carreaux_avec_points <- unlist(lapply(indices, any))
rm(indices)
carreaux_selectionnes <- carreau_light[carreaux_avec_points, ] 
rm(carreau_light)
# carreaux sélectionnés/filtrés = 147 034

# Calculer la densité de points
densite <- st_intersects(carreaux_selectionnes$geom, st_centroid(commerces$geom))
densite_com <- sapply(densite, length)  

# Afficher les résultats
head(densite_com)
units(densite_com)
hist(densite_com, main = "Densité de boulangeries dans chaque carreau")


#####################################################################################
# Calcul de la densité de commerces alimentaires par BUFFER ----
# Code Olivier Theureaux
#####################################################################################

# Dans ce code, centroids_df est un data frame contenant les coordonnées du centroïd 
# pour chaque IRIS et points_df est un data frame contenant les coordonnées des 
# commerces. Le paramètre sigma dans la fonction density.ppp() contrôle 
# la taille de la fenêtre de lissage utilisée pour calculer la densité de points. 

# Charger les bibliothèques
# Packages
pacman::p_load(sf,tidyverse,tmap,geosphere, rgeos, sp, rgdal, spatstat)

# Créer un objet sf à partir des coordonnées du centroïd
centroids_sf <-
  st_as_sf(centroids_df,
           coords = c("longitude", "latitude"),
           crs = 4326)

# Créer les buffers
buffers <- st_buffer(centroids_sf, dist = c(100, 300, 500))

# Calculer la densité de points pour chaque buffer
densities <- lapply(buffers, function(x) {
  # Convertir le buffer en objet spatstat
  x_ppp <- as(x, "owin")
  
  # Convertir les coordonnées des commerces en objet spatstat
  points_ppp <- as(points_df, "ppp")
  
  # Calculer la densité de points
  density <- density.ppp(points_ppp, sigma = 50, at = "points", correction = "border")
  
  # Retourner la densité de points
  return(density)
})
