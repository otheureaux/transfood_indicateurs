setwd("~/OT/TRANSFOOD/DATA/")
list.files()

########################################################
########################################################
########################################################
# Distance au centroïd de chaque IRIS de l'Hérault ----
# K means pour clustering

# Packages
pacman::p_load(sf,tidyverse,tmap,mapview,httr,osw4R,geosphere,rgeos,sp,rgdal,ggplot2,purrr)

# Chargement et traitements des couches

## Chargements des carreaux INSPIRE

### Sources des données
### https://www.insee.fr/fr/statistiques/4176290?sommaire=4176305

setwd("~/OT/TRANSFOOD/DATA/")
list.files()
iris <- st_read("GPKG/iris_herault.gpkg")
mapview(iris)
# 2 289 070 lignes en France Métropolitaine

## Chargement de la couche commerces
### Source : Olivier Theureaux
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg")
# commerces <- st_transform(commerces, crs = 4326)

url_bd <- "https://wxs.ign.fr/topographie/geoportail/wfs?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities"
veg_url <- parse_url(url_bd)
veg_url$query <-list(service = "wfs",
                     version = "2.0.0", # optional
                     #crs = 4326,
                     crs = 2154,
                     request = "GetFeature",
                     typename = "BDTOPO_V3:departement", # nom de la couche
                     cql_filter = c("code_insee='34'"))
                     # cql_filter = c(paste0("code_insee='",  code_insee, "'")))

veg_dep <- build_url(veg_url)
herault <- st_read(veg_dep)
herault <- st_transform(herault, 2154)
commerces<- st_intersection(herault, commerces)

### Calcul du centroïd de chaque carreau
iris$geom_centroid <- st_centroid(st_make_valid(iris))
iris_centroid <- iris %>% 
  select(INSEE_COM,geom_centroid)
  
# Calculer les distances géodésiques entre chaque logement et chaque commerce
# l'unité est en mètre
distances <- st_distance(iris_centroid$geom_centroid, commerces) # ne pas pas d'objet sf

################# J'ai comparé st_distance avec distm
# distances <- st_distance(carreau_centroid_region, commerces_region)
# J'ai testé les 2 méthodes, qui renvoient le même résultat
##########################################################################
  
# Trouver le commerce le plus proche pour chaque logement
plus_proches_commerces <- data.frame(apply(distances, MARGIN = 1, FUN = min))
colnames(plus_proches_commerces) <- c("distance")
iris_dist <- bind_cols(iris, plus_proches_commerces)

# Optimisation
## joined_data <- st_join(carreau_centroid_region, commerces_region, join = st_nearest_feature)
## distances <- distm(joined_data[, c("lon", "lat")], joined_data[, c("lon.x", "lat.x")])

# K-Means

kmeans_data <- iris_dist %>% 
  select(distance) %>% 
  st_drop_geometry()
glimpse(kmeans_data)
km.out <- kmeans(kmeans_data, centers = 3, nstart = 20)
iris_dist$cluster <- km.out$cluster

coords_iris <- st_coordinates(iris_dist$geom_centroid)
iris_dist$x <- coords_iris[,1]
iris_dist$y <- coords_iris[,2]


ggplot(iris_dist, aes(x = x, y = y, color = factor(cluster))) +
  geom_point() +
  ggtitle("Cartographic Representation of Clusters")

# Plot results using tmap

# Define colors for each cluster
colors <- c("orange", "green", "yellow")

tmap_mode("view")
tm_shape(iris_dist) +
  tm_fill(col = "cluster", palette = colors) +
  tm_layout(legend.position = c("right", "bottom"), legend.bg.color = "white") +
  tm_shape(iris_centroid) + tm_borders(col = "grey") +
  tm_shape(commerces) + tm_dots()

getwd()
st_write(iris_dist, "processed_data/iris_dit.gpkg")
# Quel est le type de commerce le plus proche du centroïd ----

joined_data <- st_join(iris_centroid$geom_centroid, commerces, join = st_nearest_feature)
sort(table(joined_data$activitePrincipaleEtablissement))
# Résultat : Restauration rapide, Restauration traditionnelle, Boulangerie, Commerce d'alimentation générale

# Où sont les boulangeries les plus proches du centroïd ----



# Quels sont les IRIS dont le centre est le plus proche d'une Boulangerie ----
# 3 classes par la méthode des k-means

# Calculer les distances entre chaque logement et chaque commerce
# l'unité est en mètre
boulangerie <- commerces %>% 
  filter(commerces$activitePrincipaleEtablissement == "10.71C")
distances <- st_distance(iris_centroid$geom_centroid, boulangerie) # ne pas pas d'objet sf

################# J'ai comparé st_distance avec distm
# distances <- st_distance(carreau_centroid_region, commerces_region)
# J'ai testé les 2 méthodes, qui renvoient le même résultat
##########################################################################

# Trouver le commerce le plus proche pour chaque logement
plus_proches_commerces <- data.frame(apply(distances, MARGIN = 1, FUN = min))
colnames(plus_proches_commerces) <- c("distance")
iris_dist <- bind_cols(iris, plus_proches_commerces)

kmeans_data <- iris_dist %>% 
  select(distance) %>% 
  st_drop_geometry()
glimpse(kmeans_data)
km.out <- kmeans(kmeans_data, centers = 3, nstart = 20)
iris_dist$cluster <- km.out$cluster

coords_iris <- st_coordinates(iris_dist$geom_centroid)
iris_dist$x <- coords_iris[,1]
iris_dist$y <- coords_iris[,2]


ggplot(iris_dist, aes(x = x, y = y, color = factor(cluster))) +
  geom_point() +
  ggtitle("Cartographic Representation of Clusters")

# Plot results using tmap

# Define colors for each cluster
colors <- c("orange", "green", "yellow")

tmap_mode("view")
tm_shape(iris_dist) +
  tm_fill(col = "cluster", palette = colors) +
  tm_layout(legend.position = c("right", "bottom"), legend.bg.color = "white") +
  tm_shape(iris_centroid) + tm_borders(col = "grey") +
  tm_shape(boulangerie) + tm_dots()

