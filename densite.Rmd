---
title: ""
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Densité de commerces alimentaires ? {.tabset .unnumbered}

## Densité par commune

### Densité rapportée à la superficie

Voici le script R qui permet de récupérer la densité de commerces pour chaque commune française. 

***

```{r, eval=F}
# Packages
pacman::p_load(sf,tidyverse,tmap,geosphere, sp, rgdal)

# Chargements des données COMMUNES
communes_data <- st_read("C:/Users/otheureaux/Documents/OT/DECOUPAGES_FR/COMMUNE.shp") %>%
  st_transform(crs = 4326) %>%
  select(NOM, INSEE_COM, POPULATION, INSEE_DEP, INSEE_REG)

communes_data_all <- communes_data

# Chargement de la couche des commerces au format geopackage
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg") %>%
  st_transform(crs = 4326) %>% 
    dplyr::filter(!grepl("^(971|972|973|974|976)", plg_code_commune)) # exclusion des caractères commençant par 971|972|973|974|976. Le ^ permet cela. 

# commerces <- commerces %>% filter(activitePrincipaleEtablissement == '10.71C')

# Fonction pour compter les commerces dans chaque COMMUNE
count_commerces <- function(communes_data, commerces) {
  indices <- st_intersects(communes_data, commerces)
  counts <- sapply(indices, length)
  return(counts)
}

# Compter le nombre de commerces dans chaque COMMUNE
commerces_count <- count_commerces(communes_data, commerces)


# Calculer la superficie de chaque COMMUNES
communes_area <- st_area(communes_data)

# Calculer la densité de commerces pour chaque ICOMMUNES
densite_km <- commerces_count / communes_area * 1000000
densite_pop <- commerces_count / communes_data$POPULATION


# Ajouter les résultats dans un dataframe
results <- data.frame(
  INSEE_COM = communes_data$INSEE_COM,
  densite_km = densite_km,
  densite_pop = densite_pop
)
head(results)

communes_data_fusion <- left_join(x = communes_data, y = results, by = "INSEE_COM") 

# Sélection communes Hérault (34)
communes_34 <- communes_data_fusion %>% 
  filter(INSEE_DEP == '34')

# st_write(communes_34, "processed_data/densite_commmerce_commune_herault.gpkg")

```


Je développe une autre méthode sur l'onglet **Diversité**. Une solution plus simple consiste tout d'abord à compter le nombre de commerces par communes. Puis de ramener ce nombre à la superficie de chaque commune. 


Cartographie de la densité de commerces par commune

<center>
![](images/densite_commerces_commune.png)
*Densité de commerces par commune*
</center>


<center>
![](images/commerces_communes_34.png)
*Densité de commerces par commune dans le département de l'Hérault*
</center>

```{r, out.width="100%", eval =F}
# Packages
pacman::p_load(sf,tidyverse,tmap,geosphere, sp, rgdal)

# Chargement de la couche des commerces
communes_data_34 <- st_read("processed_data/densite_commmerce_commune_herault.gpkg")

# Créer une carte avec tmap
tmap::tmap_mode("view")
tmap::tm_shape(communes_data_34) +
  tm_polygons('densite_km',
              title = "Densité de commerces (km²)",
              palette = "Blues", 
              style = "quantile") +
  tm_shape(communes_data_34) +
  tm_borders(lwd = 0.1, 
            col = "white") +
  tm_layout(frame = FALSE)

```


### Densité rapportée à la population de chaque commune (avec TMAP)

```{r, out.width="100%", warning=F, eval = T}

# Packages
pacman::p_load(sf,tidyverse,tmap,geosphere, sp, rgdal)

# Chargement des couches des commerces
communes_data_34 <- st_read("processed_data/densite_commmerce_commune_herault.gpkg", quiet = T)
communes_data_34_pop <- st_read("processed_data/densite_commmerce_commune_herault.gpkg", quiet = T)

communes_data_34_pop$commerces_count <- (communes_data_34_pop$POPULATION * communes_data_34_pop$densite_pop)
communes_data_34$commerces_count <- (communes_data_34$POPULATION * communes_data_34$densite_pop)

# Créer une carte avec tmap
tmap::tmap_mode("view")
tmap::tm_shape(communes_data_34) +
  tm_fill('densite_km',
              title = "Densité de commerces (km²)",
              palette = "Blues", 
              style = "quantile") +
    tm_shape(communes_data_34) + 
  tm_dots(size = 'commerces_count') +
  tm_shape(communes_data_34_pop) +
  tm_fill('densite_pop',
              title = "Densité de commerces par habitant",
              palette = "Blues", 
              style = "quantile") +
  tm_shape(communes_data_34) +
  tm_borders(lwd = 0.1, 
            col = "white") +
  tm_layout(frame = FALSE) + 
  tm_shape(communes_data_34_pop) + 
  tm_dots(size = 'commerces_count')

```

Question : combien d'habitants vivent dans des espaces faiblement dotés de commerces ?

### Densité par type de commerces

```{r, eval=F}
# Packages
pacman::p_load(sf,tidyverse,tmap,geosphere, sp, rgdal)

# Chargements des données COMMUNES
communes_data <- st_read("C:/Users/otheureaux/Documents/OT/DECOUPAGES_FR/COMMUNE.shp") %>%
  st_transform(crs = 4326) %>%
  select(NOM, INSEE_COM, POPULATION, INSEE_DEP, INSEE_REG)

communes_data_all <- communes_data

# Chargement de la couche des commerces au format geopackage
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg") %>%
  st_transform(crs = 4326) %>% 
    dplyr::filter(!grepl("^(971|972|973|974|976)", plg_code_commune)) # exclusion des caractères commençant par 971|972|973|974|976. Le ^ permet cela. 

# commerces <- commerces %>% filter(activitePrincipaleEtablissement == '10.71C')

# Fonction pour compter les commerces dans chaque COMMUNE
count_commerces <- function(communes_data, commerces) {
  indices <- st_intersects(communes_data, commerces)
  counts <- sapply(indices, length)
  return(counts)
}

# Compter le nombre de commerces dans chaque COMMUNE
commerces_count <- count_commerces(communes_data, commerces)

# Compte le nombre de commerces par type dans chaque COMMUNE

code_APE <- c(
  "10.71B",   "10.71C",   "10.71D",   "47.11A",  "47.11B",   "47.11C",  "47.11D",
  "47.11F",   "47.11E",   "47.21Z",   "47.22Z",  "47.23Z",   "47.24Z",  "47.29Z",  
  "56.10A",   "56.10B",   "56.10C") 

# i <- 1

for( i in 1:17) {
commerces_filtre <- commerces %>% filter(activitePrincipaleEtablissement == code_APE[i])
commerces_count <- count_commerces(communes_data, commerces_filtre)
communes_data_all <- cbind(communes_data_all, commerces_count)
communes_data_all <- rename(communes_data_all, !!paste0("commerces_count_", i) := commerces_count)
}

names(communes_data_all)
communes_data_all_rename <- communes_data_all %>% 
  rename(code_10.71B = commerces_count_1,   
         code_10.71C = commerces_count_2,   
         code_10.71D = commerces_count_3,   
         code_47.11A = commerces_count_4,  
         code_47.11B = commerces_count_5,  
         code_47.11C = commerces_count_6,
         code_47.11D = commerces_count_7,
         code_47.11F =commerces_count_8,   
         code_47.11E = commerces_count_9,   
         code_47.21Z = commerces_count_10,   
         code_47.22Z = commerces_count_11,  
         code_47.23Z = commerces_count_12,   
         code_47.24Z = commerces_count_13,  
         code_47.29Z = commerces_count_14,  
         code_56.10A = commerces_count_15,   
         code_56.10B = commerces_count_16,   
         code_56.10C = commerces_count_17)

# Calculer la superficie de chaque COMMUNES
communes_area <- st_area(communes_data)

# Calculer la densité de boulangerie (10.71C) pour chaque ICOMMUNES
densite_boulangerie_km <- communes_data_all_rename$code_10.71C / communes_area * 1000000
densite_boulangerie_pop <- communes_data_all_rename$code_10.71C / communes_data$POPULATION


# Ajouter les résultats dans un dataframe
results <- data.frame(
  INSEE_COM = communes_data$INSEE_COM,
  densite_km = densite_boulangerie_km,
  densite_pop = densite_boulangerie_pop
)
head(results)

communes_data_fusion <- left_join(x = communes_data, y = results, by = "INSEE_COM") 
```


### Densité par le biais de statistiques focales autour de pixels (Feuillet, 2023)

Au niveau de la France entière

```{r, out.width="100%",eval = F}
# Packages
pacman::p_load(dplyr, sf, terra, tmap)

# Chargement de la couche commerces
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg", quiet = T) %>%
  st_transform(crs = 2154) %>% 
  filter(codeCommuneEtablissement < 97000)

# Définition du raster
res <- 500

# Rasterisation de la couche vecteur des commerces
rast_commerces <- rast(commerces, resolution = res)

# Add a new field with a constant value (e.g., 1) to the point data
commerces$const_value <- 1

rastCommerces <- terra::rasterize(vect(commerces), rast_commerces, field = "const_value", background = 0)
crs(rastCommerces) <- "epsg:2154"
plot(rastCommerces)
```

```{r, out.width="100%",eval = F}
d <- 1000 # Valeur du diamètre du noyau. Cela signifie que pour chaque pixel, nous allons calculer la densité de traces GPS dans un voisinage de 1000 m. 

w <- terra::focalMat(rastCommerces, d = d, type = "circle") # circle signifie que la forme du noyau est circulaire (il pourrait rectangulaire par exemple). 

densCommerces <- terra::focal(x = rastCommerces, w = w, fun = sum) # Calcul de la statistique focale, qui correspond à notre densité de noyau.

plot(densCommerces)

```

Au niveau de l'Hérault

```{r, out.width="100%",eval = F}
# Packages
pacman::p_load(dplyr, sf, terra, tmap)

# Chargement de la couche commerces
commerces_34 <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg", quiet = T) %>%
  st_transform(crs = 2154) %>% 
  filter(departement == '34000')

# Définition du raster
res <- 500

# Rasterisation de la couche vecteur des commerces
rast_commerces <- rast(commerces_34, resolution = res)

# Add a new field with a constant value (e.g., 1) to the point data
commerces_34$const_value <- 1

rastCommerces <- terra::rasterize(vect(commerces_34), rast_commerces, field = "const_value", background = 0)
crs(rastCommerces) <- "epsg:2154"
plot(rastCommerces)
```

```{r, out.width="100%",eval = F}
d <- 1000 # Valeur du diamètre du noyau. Cela signifie que pour chaque pixel, nous allons calculer la densité de traces GPS dans un voisinage de 1000 m. 

w <- focalMat(rastCommerces, d = d, type = "circle") # circle signifie que la forme du noyau est circulaire (il pourrait rectangulaire par exemple). 

densCommerces <- focal(x = rastCommerces, w = w, fun = sum) # Calcul de la statistique focale, qui correspond à notre densité de noyau.

plot(densCommerces)

```

### Densité par la fonction density.ppp du package `spatstat`

Calcul la densité de points d'un objet en utilisant une **estimation par noyau de densité**.
Pour chaque point, et à l'intérieur d'une région (ou noyau), qui définit le voisinage, une densité d'objets est calculée (une moyenne pondérée des valeurs observées au voisinage de ce point dans un rayon prédéfini).  
Au sein de cette région, les points les plus proches sont pondérés (pondération spatiale) plus fortement en fonction d'une fonction prédéfinie (gaussienne, triangulaire, etc.).  


Limites : Les méthodes de lissage atténuent les ruptures et les frontières et induisent des représentations continues des phénomènes géographiques. Les cartes lissées font donc apparaître localement de l’autocorrélation spatiale (INSEE, Méthodes 2018)



```{r, eval = F,  out.width="100%", warning=F}
pacman::p_load(sf, spatstat, tmap, dplyr, sp, mapview)

# Charger le fichier Geopackage des commerces alimentaires
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg", quiet = T) %>%
  st_transform(crs = 2154)

# Chargement de l'emprise de l'Hérault
url <- "https://wxs.ign.fr/topographie/geoportail/wfs?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities"
parse_url <- httr::parse_url(url)
parse_url$query <-list(service = "wfs", version = "2.0.0", crs = 4326,
                     request = "GetFeature", typename = "BDTOPO_V3:departement",
                     cql_filter = c("code_insee='34'"))
departement <- st_read(httr::build_url(parse_url), quiet = T) %>% 
  st_transform(crs = 2154)

# Création de l'emprise d'étude au format OWIN : la fenêtre d'observation
dep_Owin <- as.owin(departement) # package spatstat.geom

# Extraction des coordonnées du fichiers sf
coord <- st_coordinates(commerces)

# Création de l'objet ppp avec spatstat geom
p <- spatstat.geom::ppp(coord[, 1],
                        coord[, 2],
                        window = dep_Owin)

# Fonction density.ppp sur l'objet ppp 
ds_ppp <- spatstat.explore::density.ppp(p,
                                        sigma = 10000)

# Créer le graphique
plot(ds_ppp, main = 'commerces density.ppp')
```
<center>
*Carte : Lissage géographique - Estimation par noyau*
</center>

```{r, out.width="100%", eval = F}
par(mfrow=c(2,1))  
plot(densCommerces, main = "res = 500, d = 1000m")
plot(ds_ppp, main = 'commerces density.ppp, sigma = 10000m')
```




```{r, echo = F, eval = F}
par(mfrow=c(1,1)) 
```




## Densité carreau

```{r, eval= F}
# Charger les packages nécessaires
pacman::p_load(sf, tidyverse, tmap, geosphere, sp, rgdal)

setwd("~/OT/TRANSFOOD/DATA/")

# Charger et transformer les données
carreau <- st_read("data/Filosofi2015_carreaux_200m_metropole.gpkg") %>%
  st_transform(crs = 4326) %>%
  select(IdINSPIRE, Depcom, geom)

commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg") %>%
  st_transform(crs = 4326)

# Sélection des carreaux contenant des commerces
carreaux_selectionnes <- st_join(carreau, commerces, join = st_intersects)

# Calculer le nombre de commerces par carreau
carreaux_commerces <- carreaux_selectionnes %>%
  group_by(IdINSPIRE) %>%
  summarize(nb_commerces = n(), .groups = 'drop')

# Ajouter le nombre de commerces par carreau aux données initiales
carreaux_selectionnes_final <- left_join(carreau, carreaux_commerces, by = "IdINSPIRE")

# Remplacer les valeurs manquantes par 0
carreaux_selectionnes_final$nb_commerces[is.na(carreaux_selectionnes_final$nb_commerces)] <- 0

# Calculer la densité de commerces
carreaux_selectionnes_final$densite <- carreaux_selectionnes_final$nb_commerces / st_area(carreaux_selectionnes_final)

# Sélectionner les colonnes pertinentes
carreaux_selectionnes_final <- carreaux_selectionnes_final %>%
  select(IdINSPIRE, nb_commerces, densite, geom) %>%
  as.data.frame()

# Charger le package ggplot2
library(ggplot2)

# Créer un histogramme pour montrer la distribution du nombre de commerces par carreau
ggplot(carreaux_selectionnes_final, aes(x = nb_commerces)) +
  geom_histogram(binwidth = 1, fill = "blue", color = "black") +
  labs(title = "Distribution du nombre de commerces par carreau",
       x = "Nombre de commerces",
       y = "Nombre de carreaux") +
  theme_minimal()
```


Histogramme du nombre de commerces dans chaque carreau

```{r, out.width="100%"}
pacman::p_load(dplyr)
densite <- read.csv2("processed_data/carreaux_selectionnes_final.csv")
dim(densite)

# Histogramme avec l'ensemble des données
summary(densite$nb_commerces)
hist(densite$nb_commerces, breaks = c(0,1,2,3,4,5,10,166))

# Sélection des carreaux avec un maximum de 20 commerces
densite$nb_commerces <- as.numeric(densite$nb_commerces)
densite_filter <- densite %>% 
  dplyr::filter(nb_commerces < 20)
dim(densite_filter)

summary(densite_filter$nb_commerces)
hist(densite_filter$nb_commerces, breaks = c(0,1,2,3,4,5,10,19))
```

### Densité carreau par commerces

Possibilité de sélectionner une activité parmi 17 commerces alimentaires

Liste des activités
* 10.71B | Cuisson de produit de boulangerie
* 10.71C | Boulangerie et boulangerie-pâtisserie
* 10.71D | Pâtisserie
* 47.11A | Commerce de détail de produits surgelés
* 47.11B | Commerce d'alimentation générale
* 47.11C | Supérettes
* 47.11D | Supermarchés
* 47.11F | Hypermarchés
* 47.11E | Magasins multi-commerces
* 47.21Z | Commerce de détail de fruits et légumes en magasin spécialisé
* 47.22Z | Commerce de détail de viandes et de produits à base de viande en magasin spécialisé
* 47.23Z | Commerce de détail de poissons, crustacés et mollusques en magasin spécialisé
* 47.24Z | Commerce de détail de pain, pâtisserie et confiserie en magasin spécialisé
* 47.29Z | Autres commerces de détail alimentaires en magasin spécialisé
* 56.10A | Restauration traditionnelle
* 56.10B | Cafétérias et autres libres-services
* 56.10C | Restauration de type rapide

```{r, eval= T}
# Packages
pacman::p_load(sf,tidyverse,tmap,geosphere, sp, rgdal)

##########################################
# Ici renseigner l'activité à étudier
commerces_selection <- '10.71C' # Boulangerie et boulangerie-pâtisserie
##########################################
# Fichier source
setwd("~/OT/TRANSFOOD/DATA/")
list.files()

## Chargements des carreaux INSPIRE
carreau <- st_read("C:/Users/otheureaux/Documents/OT/URBASANTE/DONNEES_VECTEUR/INSEE/INSEE_Filosofi2015_carreaux_200m_metropole.gpkg")
mapview::mapview(st_make_valid(st_as_sf(carreau)))
tm_shape(carreau) + tm_borders()
carreau <- st_transform(carreau, crs = 4326)
carreau_light <- carreau %>% 
  select(IdINSPIRE, Depcom, geom)
rm(carreau)
# 2 289 070 lignes en France Métropolitaine
#st_write(carreau_light, "Filosofi2015_carreaux_200m_metropole_light.gpkg")

## Chargement de la couche commerces
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg")
commerces <- st_transform(commerces, crs = 4326)
commerces <- commerces%>% 
  filter(activitePrincipaleEtablissement == commerces_selection)

### Sélection des carreaux contenant des commerces
indices <- st_intersects(carreau_light, commerces)
carreaux_avec_points <- unlist(lapply(indices, any))
rm(indices)
carreaux_selectionnes <- carreau_light[carreaux_avec_points, ] 
rm(carreau_light)
# carreaux sélectionnés/filtrés = 147 034

# Calculer le nombre de points dans chaque carreau
densite <- st_intersects(carreaux_selectionnes$geom, st_centroid(commerces$geom))
densite_com <- sapply(densite, length) # cette fonction calcul le nombre de commerces dans chaque carereau

# Afficher les résultats
head(densite_com)
hist(densite_com, main = "Nombre de boulangeries dans chaque carreau")

# Ajouter le nombre de commerces au tableau des carreaux
carreaux_selectionnes$nb_commerce <- densite_com
carreaux_selectionnes <- carreaux_selectionnes %>% 
  rename("10.71C" = nb_commerce)
```


## Densité par IRIS

```{r, eval=F}
# Packages
pacman::p_load(sf,tidyverse,tmap,geosphere, sp, rgdal)

# Fichier source
setwd("~/OT/TRANSFOOD/DATA/")
list.files()

## Chargements des données IRIS
iris_data <- st_read("path/to/your/iris_data.shp") %>%
  st_transform(crs = 4326) %>%
  select(CODE_IRIS, NOM_IRIS, DEPCOM, geom)

## Chargement de la couche commerces
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg") %>%
  st_transform(crs = 4326)

# Fonction pour compter les commerces dans chaque IRIS
count_commerces <- function(iris, commerces) {
  indices <- st_intersects(iris, commerces)
  counts <- sapply(indices, length)
  return(counts)
}

# Compter le nombre de commerces dans chaque IRIS
commerces_count <- count_commerces(iris_data, commerces)

# Calculer la superficie de chaque IRIS
iris_area <- st_area(iris_data)

# Calculer la densité de commerces pour chaque IRIS
density_commerces <- commerces_count / iris_area

# Ajouter les résultats dans un dataframe
results <- data.frame(
  CODE_IRIS = iris_data$CODE_IRIS,
  NOM_IRIS = iris_data$NOM_IRIS,
  DEPCOM = iris_data$DEPCOM,
  density_commerces = density_commerces
)

head(results)

```




## Densité par buffer

```{r, eval=F}
# Packages
pacman::p_load(sf,tidyverse,tmap,geosphere, sp, rgdal)

# Fichier source
setwd("~/OT/TRANSFOOD/DATA/")
list.files()

## Chargements des carreaux INSPIRE
carreau <- st_read("data/Filosofi2015_carreaux_200m_metropole.gpkg") %>%
  st_transform(crs = 4326) %>%
  select(IdINSPIRE, Depcom, geom)

## Chargement de la couche commerces
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg") %>%
  st_transform(crs = 4326)

# Calculer les centroïdes des carreaux
carreau_centroid <- carreau %>%
  st_centroid()

# Créer des buffers de 500m, 1000m et 5000m autour des centroïdes
buffer_500 <- st_buffer(carreau_centroid, dist = 500)
buffer_1000 <- st_buffer(carreau_centroid, dist = 1000)
buffer_5000 <- st_buffer(carreau_centroid, dist = 5000)

# Fonction pour compter les commerces dans un buffer pour chaque carreau
count_commerces <- function(buffer, commerces) {
  indices <- st_intersects(buffer, commerces)
  counts <- sapply(indices, length)
  return(counts)
}

# Compter le nombre de commerces dans chaque buffer pour chaque carreau
count_500 <- count_commerces(buffer_500, commerces)
count_1000 <- count_commerces(buffer_1000, commerces)
count_5000 <- count_commerces(buffer_5000, commerces)

# Calculer la densité de commerces pour chaque buffer
area_500 <- st_area(buffer_500)
area_1000 <- st_area(buffer_1000)
area_5000 <- st_area(buffer_5000)

density_500 <- count_500 / area_500
density_1000 <- count_1000 / area_1000
density_5000 <- count_5000 / area_5000

# Ajouter les résultats dans un dataframe
results <- data.frame(
  IdINSPIRE = carreau$IdINSPIRE,
  Depcom = carreau$Depcom,
  density_500 = density_500,
  density_1000 = density_1000,
  density_5000 = density_5000
)

head(results)
```


## KDE

**Estimation de Densité par Noyau (KDE)**  

* Objectif : L'estimation de densité par noyau est une technique utilisée pour estimer la fonction de densité de probabilité d'un ensemble de données. En statistique spatiale, elle est souvent utilisée pour estimer la densité de points sur une carte, permettant de visualiser où les points (ou événements) se produisent le plus fréquemment.  

* Calcul : La KDE fonctionne en plaçant un noyau (une fonction de lissage, comme une distribution gaussienne) sur chaque point de données, puis en sommant ces fonctions de noyau sur l'ensemble de l'espace d'étude pour obtenir une surface de densité continue.    

* Interprétation : Les zones à haute densité indiquées par la KDE montrent où les points ou les événements se concentrent, tandis que les zones à faible densité montrent où ils sont relativement rares.  


**Exemple** : Densité par la méthode des noyaux sur le Calvados  

```{r, eval=F}

pacman::p_load(sf, spatstat, tmap, dplyr, sp, goftest, mapview)

# Charger le fichier gpkg des commerces alimentaires
setwd("~/OT/TRANSFOOD/DATA/")
list.files("C:/Users/otheureaux/Documents/OT/DECOUPAGES_FR/")

commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg") %>%
  st_transform(crs = 2154) # %>% 
  #filter(activitePrincipaleEtablissement== '10.71D') # Pâtisserie

## Chargements des données COMMUNES
departement <- st_read("C:/Users/otheureaux/Documents/OT/DECOUPAGES_FR/DEPARTEMENT.shp") %>%
  st_transform(crs = 2154)  %>% 
  filter(grepl("^14", INSEE_DEP))

mapview(departement)

# Création de l'emprise d'étude au format OWIN : la fenêtre d'observation
calvados_Owin <- as.owin(departement) # package spatstat.geom

# Extraction des coordonnées du fichiers sf
coord <- st_coordinates(commerces)

# Création de l'objet ppp avec spatstat geom
p <- spatstat.geom::ppp(coord[, 1],
                        coord[, 2],
                        window = calvados_Owin)

# Fonction densité.ppp sur l'objet ppp 
ds_ppp <- spatstat.explore::density.ppp(p)

# Créer le graphique
plot(ds_ppp, main = 'commerces density.ppp')

# Spécifier le fichier de sortie PNG et les options
png("images/KDE_Calvados.png", width=800, height=600, res=120)

# Tracer le graphique à nouveau pour qu'il soit enregistré dans le fichier PNG
plot(ds_ppp, main = 'commerces density.ppp')

# Fermer la sortie PNG
dev.off()
```


<center>
![](images/KDE_Calvados.png)
</center>
