# PACKAGES

library(raster)
library(terra)
library(stars)
library(sf)
library(qgisprocess) # Permet d'utiliser Geomorphon depuis QGIS
library(tidyverse)

# 1 - Selection par localisation : création de la 5e classe : pixels colonisables ----

## Boucle pour intégrer les pixels colonisables mais non colonisés ----
# Point de départ : les rasters diachroniques 4 classes
# Point d'arriver : le nouveau raster 5 classes avec la classe pixel non colonisé codé 100

setwd("C:/Users/thier/Documents/OT/GEO/GEO/MASTER/M2/SPATIAL_TREE_P/RASTER/classif_supervisee/allparcelles")
for(i in i:656){
  raster <- rast(paste0("raster_diachronie_ngb_res1_", tout_names[i], "_.tif"))
  names(raster) <- "classif"
  r <- as.points(raster) %>%
    st_as_sf()
    r_0 <- r %>%
    filter(classif == "0") %>%
    st_as_sf(r_0)
  
  r_11 <- r %>%
    filter(classif == "11") %>%
    st_as_sf(r_11)
  distance_shp_boucle <- qgis_run_algorithm("qgis:distancetonearesthubpoints", 
                                            INPUT = r_0,
                                            HUBS = r_11,
                                            FIELD = "classif",
                                            UNIT = 0
  )
  
  distance_shp_boucle <- st_as_sf(distance_shp_boucle)
  distance_inf_0 <- distance_shp_boucle %>%
    filter(distance_shp_boucle$HubDist < 30) # choix de la distance maximale
  distance_inf_0 <- mutate(distance_inf_0, new_value = 100)
  distance_inf_0$classif <- as.integer(distance_inf_0$classif)
  
  raster_new <- qgis_run_algorithm("gdal:rasterize_over", 
                                   INPUT = distance_inf_0,
                                   INPUT_RASTER = raster,
                                   FIELD = "new_value", 
                                   ADD = 0)
}

## Modification de la résolution des rasters diachronique 0.5 => 1 ----
# Pour insérer les pixels diachroniques dans le stack il faut modifier sa résolution
for(i in 1:656){
  raster <- rast(paste0("raster_diachronie_ngb_", tout_names[i], "_.tif"))
  raster <- aggregate(raster, fact = 2, fun = "modal" )
  writeRaster(raster, paste("raster_diachronie_ngb_res1", tout_names[i], ".tif", sep = "_"))
  
  
# 2 - Analyse PIXELS  ----
########################################################################-
  

## Réalignement des rasters "diachronie" pour qu'ils correspondent aux autres rasters ----
  
  for (i in 1:656){
    x <- rast(paste0("raster_diachronie_ngb_res1_", tout_names[i], "_.tif"))
    y <- rast(paste0("geomorphon_L30_res1m_", tout_names[i], "_.tif"))
    z <- resample(x,y, method="ngb") # fonction resample = réalignement (nearest neighbor)
    writeRaster(raster(z), paste("raster_diachronie_ngb_res1_resample_", tout_names[i], ".tif", sep = "_"))
  }
?resample

## Créer un stack de rasters et extraire les données ----

# Aggregate + stack pour l'ensemble des rasters. Il faut lancer 2 fois : 1 fois pour donner quali et une fois pour les données quanti
  for(i in 1:656){
    donnees <- list.files(pattern = paste0("_", tout_names[i], "_"))
    stack <- stack(donnees) 
    stack <- aggregate(stack, fact = 10, fun = "median") # variables qualitatives
    donnees_stack <- raster::extract(stack, c(1:ncell(stack)))
    donnees_stack <- na.omit(donnees_stack)
    
    colnames(donnees_stack) <- c("aspect", "geom_30", "altitude", "MPI", "pente", 
                                 "diachronie", "SVF", "TPI", "TRI", "valley_depth", 
                                 "visible_sky", "WEI", "TWI")
    write.csv(donnees_stack, file = paste0("donnees_stack_median_", tout_names[i], ".csv"))
  }
  
  ## Récupérer l'ensemble des données et les enregister dans un seul tableau ----
  setwd("C:/Users/thier/Documents/OT/GEO/GEO/MASTER/M2/SPATIAL_TREE_P/RASTER/parcelles/1m/all2")
  tableau <- tibble()
  empty_df <- tibble()
  for(i in 1:656){
    tableau <- read.csv2(paste0("donnees_stack_median_", tout_names[i],".csv"), sep = ",", dec = ".")
    empty_df <- bind_rows(empty_df, tableau)
  }
  write.csv(empty_df, file = "tableau_final_v2_median.csv")
}

# Lecture du CSV, tableau de contingence et text du Chi-deux

setwd("C:/Users/thier/Documents/OT/GEO/GEO/MASTER/M2/SPATIAL_TREE_P/RASTER/parcelles/1m/all2")
tableau <- read.csv2("Tableau_final_pixel.csv")

geomorphon <- tableau[,c(4)]
diachronie <- tableau[,c(8)]
contingence <- table(diachronie, geomorphon)
dim(contingence)
contingence_chi2 <- contingence[c(3,5),c(2,3,4,5,6,7,8,9,10)] # on sort 1 (pas assez de données)
chisq.test(contingence_chi2)


# Récupérer les coordonnées


for(i in 1:656){
  donnees <- list.files(pattern = paste0("_", tout_names[1], "_"))
  stack <- stack(donnees) 
  stack <- aggregate(stack, fact = 10, fun = "median")
  
  plot(stack)
  dim(stack)
  ncell(stack)
  
  geoloc_unique <- tibble()
  geoloc_all <- tibble()
  
  for(k in 1:ncell(stack)){
    geoloc_unique <- xyFromCell(stack, k) %>%
      set_names(c("Lat", "Long"))
    geoloc_all <- bind_rows(geoloc_all, geoloc_unique)
  }
  write.csv(geoloc_all, file = paste0("geoloc_pixel_", tout_names[i], ".csv"))
}



