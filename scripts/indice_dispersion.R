
##########################################################
# indice de dispersion spatiale au plus proche voisin R ----
# 0 concentration ; 1 répartition aléatoire ; 2 maille régulière
# Feuillet, 2019 page 87
# Code : Olivier Theureaux
##########################################################

##########################################################
# Indice de dispersion spatiale (Nearest Neighbor Index, NNI). 
##########################################################

# L'indice de dispersion spatiale mesure la distance moyenne entre chaque point et son voisin le plus 
# proche et la compare à la distance moyenne attendue pour un processus de Poisson homogène 
# (distribution spatiale aléatoire) dans la même zone.

# Note : La fonction K de Ripley mesure la probabilité qu’un point soit à une distance 
# donnée d’un autre point dans l’ensemble de points géoréférencés. La principale différence 
# entre les deux mesures est que l’indice de dispersion spatiale au plus proche voisin 
# ne prend en compte que le point le plus proche, tandis que la fonction K de Ripley prend 
# en compte tous les points dans un rayon donné.

# Objectif : 

# L'indice de dispersion spatiale, évalue la distribution spatiale globale des points en comparant la 
# distance moyenne entre les voisins les plus proches à la distance attendue sous un modèle de distribution aléatoire.





############################################################################
# Montpellier ----

### Renseigner ici le code INSEE de la commune
code_insee <- 34172
###############################################

pacman::p_load(sf, tidyverse, spatstat, httr, mapview)
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg")
commerces_mtp <- commerces %>% 
  filter(codeCommuneEtablissement == code_insee)
coord <- st_coordinates(commerces_mtp)
coord <- unique(coord)

url_bd <- "https://wxs.ign.fr/topographie/geoportail/wfs?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities"
veg_url <- parse_url(url_bd)
veg_url$query <-list(service = "wfs",
                     version = "2.0.0", # optional
                     #crs = 4326,
                     crs = 2154,
                     request = "GetFeature",
                     typename = "BDTOPO_V3:commune", # nom de la couche
                     # cql_filter = c("code_insee='34172'")
                     cql_filter = c(paste0("code_insee='",  code_insee, "'")))

veg_dep <- build_url(veg_url)
mtp_sf <- st_read(veg_dep)
mtp_sf <- st_transform(mtp_sf, 2154)
mtp_sf_bbox <- st_bbox(mtp_sf)
mtp_sf_Owin <- as.owin(mtp_sf_bbox) 

p <- spatstat.geom::ppp(coord[, 1],
                        coord[, 2],
                        window = mtp_sf_Owin)

# distance entre chaque point et le bord le plus proche de la fenêtre spatiale

mtp_sf_Owin <- edges(mtp_sf_Owin)
distances <- nncross(X = p, Y = mtp_sf_Owin)$dist

# Calculer la moyenne des distances

mean_dist <- mean(distances)

# Calculer la distance moyenne attendue

n <- length(p)
mtp_sf_bbox <- st_bbox(mtp_sf)
mtp_sf_Owin <- as.owin(mtp_sf_bbox) 
mean_dist_expected <- 0.5 * sqrt(area.owin(mtp_sf_Owin) / n)

# Indice de dispersion spatiale: Comparaison des deux moyennes

NNI <- mean_dist / mean_dist_expected

### Résultat : DISPERSION ###
#############################

############################################################################
# Moret-sur-Loing ----

### Renseigner ici le code INSEE de la commune
code_insee <- 77316
###############################################

pacman::p_load(sf, tidyverse, spatstat, httr)
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg")
commerces_mtp <- commerces %>% 
  filter(codeCommuneEtablissement == code_insee)
coord <- st_coordinates(commerces_mtp)
coord <- unique(coord)

url_bd <- "https://wxs.ign.fr/topographie/geoportail/wfs?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities"
veg_url <- parse_url(url_bd)
veg_url$query <-list(service = "wfs",
                     version = "2.0.0", # optional
                     #crs = 4326,
                     crs = 2154,
                     request = "GetFeature",
                     typename = "BDTOPO_V3:commune", # nom de la couche
                     # cql_filter = c("code_insee='77316'"))
                     cql_filter = c(paste0("code_insee='",  code_insee, "'")))
veg_dep <- build_url(veg_url)
mtp_sf <- st_read(veg_dep)
mtp_sf <- st_transform(mtp_sf, 2154)
mtp_sf_bbox <- st_bbox(mtp_sf)
mtp_sf_Owin <- as.owin(mtp_sf_bbox) 

p <- spatstat.geom::ppp(coord[, 1],
                        coord[, 2],
                        window = mtp_sf_Owin)

mtp_sf_Owin <- edges(mtp_sf_Owin)
distances <- nncross(X = p, Y = mtp_sf_Owin)$dist

# Calculer la moyenne des distances
mean_dist <- mean(distances)

# Calculer la distance moyenne attendue
n <- length(p)
mtp_sf_bbox <- st_bbox(mtp_sf)
mtp_sf_Owin <- as.owin(mtp_sf_bbox) 
mean_dist_expected <- 0.5 * sqrt(area.owin(mtp_sf_Owin) / n)

# Comparer les deux moyennes
NNI <- mean_dist / mean_dist_expected

#### REPARTITON ALEATOIRE ###
#############################

############################################################################
# Caen ----
#################################

### Renseigner ici le code INSEE de la commune
code_insee <- 14118
###############################################

pacman::p_load(sf, tidyverse, spatstat, httr)
commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg")
commerces_mtp <- commerces %>% 
  filter(codeCommuneEtablissement == code_insee)
coord <- st_coordinates(commerces_mtp)
coord <- unique(coord)

url_bd <- "https://wxs.ign.fr/topographie/geoportail/wfs?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities"
veg_url <- parse_url(url_bd)
veg_url$query <-list(service = "wfs",
                     version = "2.0.0", # optional
                     #crs = 4326,
                     crs = 2154,
                     request = "GetFeature",
                     typename = "BDTOPO_V3:commune", # nom de la couche
                     # cql_filter = c("code_insee='77316'"))
                     cql_filter = c(paste0("code_insee='",  code_insee, "'")))
veg_dep <- build_url(veg_url)
mtp_sf <- st_read(veg_dep)
mtp_sf <- st_transform(mtp_sf, 2154)
mtp_sf_bbox <- st_bbox(mtp_sf)
mtp_sf_Owin <- as.owin(mtp_sf_bbox) 

p <- spatstat.geom::ppp(coord[, 1],
                        coord[, 2],
                        window = mtp_sf_Owin)

mtp_sf_Owin <- edges(mtp_sf_Owin)
distances <- nncross(X = p, Y = mtp_sf_Owin)$dist

# Calculer la moyenne des distances
mean_dist <- mean(distances)

# Calculer la distance moyenne attendue
n <- length(p)
mtp_sf_bbox <- st_bbox(mtp_sf)
mtp_sf_Owin <- as.owin(mtp_sf_bbox) 
mean_dist_expected <- 0.5 * sqrt(area.owin(mtp_sf_Owin) / n)

# Comparer les deux moyennes
NNI <- mean_dist / mean_dist_expected

#### REPARTITON ALEATOIRE ###
#############################


##############################################
# Modèles de covariance et de variogramme ----
##############################################

"
Quel modèle utiliser pour remplacer le modèle de Poisson homogène ?

Modèle aléatoire / Poisson inhomogène : Si le variogramme expérimental est plat et ne montre 
pas de dépendance spatiale, cela suggère que les données sont distribuées de manière aléatoire. 
Dans ce cas, un modèle de Poisson inhomogène pourrait être approprié.

Modèle de Gibbs : Si le variogramme montre une dépendance spatiale positive à courte distance et 
qu'il atteint rapidement un plateau (valeur de sill), cela peut indiquer un regroupement à petite 
échelle. Le modèle de Gibbs pourrait être utilisé pour modéliser des situations où les points sont 
regroupés en raison d'interactions locales, comme la compétition ou l'attraction entre les points.

Modèle de Neyman-Scott / Modèle de Thomas : Si le variogramme montre une dépendance spatiale à 
différentes échelles, cela pourrait indiquer un processus de regroupement multi-échelle ou un 
processus de cluster. Les modèles de Neyman-Scott et de Thomas sont des modèles de processus 
ponctuels basés sur des clusters et pourraient être utilisés pour modéliser des situations où 
les points sont regroupés en grappes ou en sous-ensembles de points.
"




######################################
# Fonction K de Ripley ----
######################################

"
Méthode d'analyse spatiale pour étudier la distribution des commerces 
alimentaires : fonction K de Ripley
"
# Charger les packages
pacman::p_load(sf,spatstat,sp,rgdal)

# Charger le fichier gpkg des commerces alimentaires
setwd("~/OT/TRANSFOOD/DATA/")

commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg") %>%
  st_transform(crs = 4326)

# Transformer les données en un format compatible avec le package 'gstat'
commerces_sp <- as(commerces, "Spatial")

# Extraire les coordonnées des commerces
commerces_coords <- coordinates(commerces_sp)

# Obtenir les limites des coordonnées
commerces_bbox <- bbox(commerces_sp)

# Créer une fenêtre rectangulaire pour définir la zone d'étude
study_area <- as.owin(list(xrange = commerces_bbox[1,], yrange = commerces_bbox[2,]))

# Créer un objet 'ppp' avec les coordonnées et la fenêtre
commerces_ppp <- ppp(commerces_coords[, 1], commerces_coords[, 2], window = study_area)

# Calculer la fonction K de Ripley
K <- Kest(commerces_ppp, correction = "border")

# Tracer la fonction K de Ripley
plot(K, main = "Fonction K de Ripley pour les commerces alimentaires")

"
Voici une comparaison entre l'indice de dispersion du plus proche voisin (Nearest Neighbor Index, NNI) 
et la fonction K de Ripley :

Objectif :

NNI : Le NNI est un indice qui évalue la tendance à la dispersion, la regroupement ou la 
distribution aléatoire des points dans un espace. Il se base sur la distance moyenne entre chaque 
point et son voisin le plus proche.

Fonction K de Ripley : La fonction K de Ripley est une mesure de la dépendance spatiale d'un 
ensemble de points à différentes échelles. Elle tient compte de la distribution des points voisins 
à différentes distances et permet d'analyser les tendances de regroupement ou de dispersion à 
plusieurs échelles.

Échelle :

NNI : Le NNI se base sur une seule échelle, celle de la distance entre chaque point et son 
voisin le plus proche. Il ne permet pas de déterminer si les tendances de regroupement ou de 
dispersion varient en fonction de l'échelle.

Fonction K de Ripley : La fonction K de Ripley permet d'analyser la dépendance spatiale à 
différentes échelles en examinant la distribution des points voisins à différentes distances. 
Elle peut révéler des tendances de regroupement ou de dispersion qui varient en fonction de l'échelle.

Correction des effets de bord :
  
NNI : Le NNI peut être sensible aux effets de bord, c'est-à-dire que les points situés près 
des bords de la zone d'étude peuvent fausser les résultats. Pour corriger cela, on peut utiliser 
des méthodes de correction des effets de bord, comme la correction de Clark-Evans.

Fonction K de Ripley : La fonction K de Ripley incorpore généralement une correction des 
effets de bord, telle que la correction de la fenêtre isotrope, qui ajuste les résultats 
en fonction de la distance entre les points et les bords de la zone d'étude.
Interprétation :

NNI : Le NNI est généralement comparé à un indice de référence (comme celui d'un processus
de Poisson homogène) pour déterminer si la distribution des points est regroupée (NNI < 1), 
aléatoire (NNI ≈ 1) ou dispersée (NNI > 1).

Fonction K de Ripley : L'interprétation de la fonction K de Ripley consiste à comparer la 
fonction K observée à la fonction K attendue sous un modèle de référence (comme un processus 
de Poisson homogène). Si la fonction K observée est supérieure à la fonction K attendue, 
cela indique un regroupement des points. Si elle est inférieure, cela indique une dispersion. 
Si les deux sont similaires, cela suggère une distribution aléatoire.

En résumé, le NNI est une mesure simple de la tendance au regroupement ou à la dispersion des
points à une seule échelle, tandis que la fonction K de Ripley est une mesure plus sophistiquée 
qui permet d'analyser la dépendance spatiale à différentes échelles et de corriger les effets de bord.
"

"
Le K de Ripley est une statistique spatiale qui permet d'analyser la distribution d'événements 
ponctuels dans un espace géographique. Concrètement, il mesure la densité de points autour de chaque 
point d'un ensemble de données et compare cette densité à celle attendue si les points étaient 
distribués de manière aléatoire. Cette statistique est utilisée pour identifier les tendances de 
regroupement (agrégation) ou de dispersion des points dans l'espace.

Le K de Ripley est calculé à l'aide d'un paramètre, généralement noté t, qui représente une
distance circulaire autour de chaque point. Pour chaque distance t, la statistique K(t) est 
calculée comme suit :
  
Pour chaque point de l'ensemble de données, on compte le nombre de points supplémentaires 
présents dans un cercle de rayon t autour de ce point.
On calcule la moyenne de ces nombres pour tous les points de l'ensemble de données.
On normalise cette moyenne en la divisant par la densité de points globale, qui est calculée 
comme le nombre total de points divisé par la surface totale de l'espace d'étude.
Le K de Ripley est souvent représenté sous forme graphique, avec t en abscisse et K(t) en ordonnée. 
La courbe K(t) est alors comparée à celle attendue sous l'hypothèse d'une distribution aléatoire 
des points. Si la courbe K(t) est supérieure à la courbe de référence, cela indique un regroupement 
des points, tandis qu'une courbe K(t) inférieure indique une dispersion des points.

En résumé, le K de Ripley permet d'analyser la distribution spatiale des points en mesurant la 
densité locale de points à différentes échelles (distances t) et en la comparant à celle attendue 
si les points étaient distribués de manière aléatoire.
"

################################################################################
pacman::p_load(sf, spatstat, rgeos, tmap, dplyr, sp, rgeos, mapview)

# Charger le fichier gpkg des commerces alimentaires
setwd("~/OT/TRANSFOOD/DATA/")
list.files("C:/Users/otheureaux/Documents/OT/DECOUPAGES_FR/")

commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg") %>%
  st_transform(crs = 2154) # %>% 
  #filter(activitePrincipaleEtablissement== '10.71D') # Pâtisserie

## Chargements des données COMMUNES
departement <- st_read("C:/Users/otheureaux/Documents/OT/DECOUPAGES_FR/DEPARTEMENT.shp") %>%
  st_transform(crs = 2154)  %>% 
  filter(grepl("^77", INSEE_DEP))
mapview(departement)



for(i in 1:nrow(departement)) {
  departement_i <- departement[i, ]

  # Intersection des commerces et des communes
  commerces_departement <- st_intersection(departement_i, commerces)
   if (nrow(commerces_departement) > 0) {
     
    # Extraire les coordonnées des commerces dans la commune
    commerces_coords <- st_coordinates(commerces_departement)

    # Créer un objet ppp avec les coordonnées des commerces dans la commune
    departement_owin <- as.owin(departement_i)
    commerces_ppp <- ppp(commerces_coords[, 1], commerces_coords[, 2], window = departement_owin)
    
    # Calculer l'indice K de Ripley
    K <- spatstat.explore::Kest(commerces_ppp, correction = "border")
    }
}

########################################################
plot(K)
########################################################
st_write(communes, "processed_data/k_ripley_commune10.gpkg")


#### 
# V2 R MAX



pacman::p_load(sf, spatstat, rgeos, tmap, dplyr, sp, rgeos, mapview)

# Charger le fichier gpkg des commerces alimentaires
setwd("~/OT/TRANSFOOD/DATA/")

commerces <- st_read("processed_data/geoloc_sans_NA_light_sf.gpkg") %>%
  st_transform(crs = 32632) %>% 
  filter(activitePrincipaleEtablissement== '10.71D')

## Chargements des données COMMUNES
communes_mtp <- st_read("C:/Users/otheureaux/Documents/OT/DECOUPAGES_FR/COMMUNE.shp") %>%
  st_transform(crs = 32632) %>% 
  filter(grepl("^34", INSEE_COM))
  # filter(grepl("34172", INSEE_COM))

K_values2 <- numeric(length(communes))
K_theo2 <- numeric(length(communes))
K_norm2 <- numeric(length(communes))

nrow(communes)

for(i in 1:10) {
  commune_i <- communes[i, ]
  
  # Intersection des commerces et des communes
  commerces_commune <- st_intersection(commune_i, commerces)
  centroids_i <- st_centroid(commune_i)
  
  if (nrow(commerces_commune) > 0) {
    # Extraire les coordonnées des commerces dans la commune
    commerces_coords <- st_coordinates(commerces_commune)
    coords_i <- st_coordinates(centroids_i)
    
    # Créer un objet ppp avec les coordonnées des commerces dans la commune
    commune_owin <- as.owin(commune_i)
    commerces_ppp <- ppp(commerces_coords[, 1], commerces_coords[, 2], window = commune_owin)
    
    # Calculer l'indice K de Ripley
    K <- spatstat.explore::Kest(commerces_ppp, correction = "border") # résultats de l'estimation de l'indice de Ripley K 
    K_values2[i] <- K$r[length(K$r)] # valeurs observées
    print(K$r)
    
    K_theo2[i] <- K$theo[length(K$theo)] # K théorique
    K_norm2[i] <- K_values2[i]  / K_theo2[i]
    
  } else {
    K_values2[i] <- NA
    K_theo2[i] <- NA
    K_norm2[i] <- NA
  }
}

plot(K)
communes$K_values <- K_values2
communes$K_theo <- K_theo2
communes$K_norm <- K_norm2

# V3 R est fixé à environ 1000m

K_values_100 <- numeric(length(communes))
K_theo_100 <- numeric(length(communes))
K_norm_100 <- numeric(length(communes))

for(i in 1:nrow(communes)) {
  commune_i <- communes[i, ]
  
  # Intersection des commerces et des communes
  commerces_commune <- st_intersection(commune_i, commerces)
 
    if (nrow(commerces_commune) > 0) {
    # Extraire les coordonnées des commerces dans la commune
    commerces_coords <- st_coordinates(commerces_commune)
    
    # Créer un objet ppp avec les coordonnées des commerces dans la commune
    commune_owin <- as.owin(commune_i)
    commerces_ppp <- ppp(commerces_coords[, 1], commerces_coords[, 2], window = commune_owin)
    
    # Calculer l'indice K de Ripley
    K <- spatstat.explore::Kest(commerces_ppp) 
    index <- which.min(abs(K$r - 150)) # Trouver l'indice de la valeur r la plus proche de 150
    print(index)
    K_values_100[i] <- K$r[index] # Valeurs observées à r = 100
    K_theo_100[i] <- K$theo[index] # K théorique à r = 100
    K_norm_100[i] <- K_values_100[i] / K_theo_100[i]
    
  } else {
    K_values_100[i] <- NA
    K_theo_100[i] <- NA
    K_norm_100[i] <- NA
  }
}


communes$K_values <- K_values_100
communes$K_theo <- K_theo_100
communes$K_norm <- K_norm_100

# Ressources : 

Fortin, M.J., & Dale, M.R.T. (2009). Analyse spatiale : concepts et méthodes. De Boeck Supérieur.
Allard, D., & Monestiez, P. (2011). Spatial statistics and modeling. Springer Science & Business Media.
Gaetan, C., & Guyon, X. (2010). Statistique spatiale. Springer.



######################################
# Indice de Moran ----
######################################
"
Les valeurs élevées de l'indice de Moran local indiquent une agglomération, 
tandis que les valeurs faibles indiquent une dispersion.
"

install.packages(c("spdep", "sf", "dplyr", "tmap"))
library(spdep)
library(sf)
library(dplyr)
library(tmap)


communes_data <- st_read("path/to/your/communes_data.shp") %>%
  st_transform(crs = 4326) %>%
  select(CODE_COMMUNE, NOM_COMMUNE, DEPCOM, geom)

commerces <- st_read("path/to/your/commerces_data.shp") %>%
  st_transform(crs = 4326)



commerces_count <- count_commerces(communes_data, commerces)
communes_data$commerces_count <- commerces_count

??Kest

communes_nb <- poly2nb(communes_data, queen = TRUE)
communes_listw <- nb2listw(communes_nb, style = "W", zero.policy = TRUE)



moran_global <- moran.test(communes_data$commerces_count, communes_listw)

moran_local <- localmoran(communes_data$commerces_count, communes_listw, zero.policy = TRUE)
communes_data$local_moran <- moran_local[, 1]
communes_data$local_pvalue <- moran_local[, 5]

tm_shape(communes_data) +
  tm_fill("local_moran", style = "quantile", n = 5, palette = "RdBu", title = "Indice de Moran Local") +
  tm_layout(legend.outside = TRUE)






###############################  

"
Analyse de la variabilité spatiale : Utilisez les variogrammes pour analyser 
la variabilité spatiale et la dépendance spatiale entre les commerces ou les 
communes. Cette méthode vous aidera à comprendre comment les valeurs d'une 
variable (par exemple, le chiffre d'affaires des commerces) varient en fonction 
de la distance entre les points.

Analyse de la répartition des commerces par catégorie : Vous pouvez examiner 
la répartition des différents types de commerces (par exemple, les restaurants, 
les magasins de vêtements, les épiceries) dans chaque commune et comparer 
leur densité et leur distribution spatiale.

Analyse de la co-localisation : Évaluez la co-localisation des commerces pour 
déterminer si certains types de commerces ont tendance à se regrouper 
géographiquement. Par exemple, vous pouvez analyser si les restaurants se 
trouvent généralement à proximité des hôtels ou des centres commerciaux.

Analyse des clusters spatiaux : Utilisez des méthodes comme le test de Moran, 
l'indice de Geary ou le test de Getis-Ord pour identifier les clusters spatiaux 
de commerces ou de communes ayant des caractéristiques similaires (par exemple, 
un chiffre d'affaires élevé, une densité de population similaire).

Modèles de régression spatiale : Envisagez d'utiliser des modèles de régression 
spatiale pour étudier les relations entre les variables spatiales (par exemple, 
le revenu des habitants, la population) et les caractéristiques des commerces 
(par exemple, le chiffre d'affaires, la taille). Les modèles de régression 
spatiale tiennent compte de la dépendance spatiale entre les observations, 
ce qui peut améliorer la précision des prédictions.

Accessibilité et analyse du réseau : Examinez l'accessibilité des commerces 
pour les résidents des communes en utilisant des analyses de réseau, par 
exemple en calculant la distance ou le temps de trajet entre les résidences 
et les commerces.
"









