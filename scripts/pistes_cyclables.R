# Chargement des packages
pacman::p_load(osmdata, sf, dplyr, mapview, httr)


# Chargement de tous les départements France Métropole
url_bd <- "https://wxs.ign.fr/topographie/geoportail/wfs?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities"
veg_url <- parse_url(url_bd)
veg_url$query <-list(service = "wfs", version = "2.0.0", # optional
                     crs = 4326,
                     request = "GetFeature",
                     typename = "BDTOPO_V3:departement")
                     # cql_filter = c("code_insee='09'"))
departements <- st_read(build_url(veg_url))
departements_metro <- departements %>% 
  filter(!grepl("^(971|972|973|974|976)", code_insee))

mapview(departements_metro)

# Initialisation de l'objet `sf` pour stocker les pistes cyclables de tous les départements
all_all_cycleways <- tibble()

# Parcourez chaque département métropolitain
for(i in 1:nrow(departements_metro)) {

# Spécification de la requête pour extraire les pistes cyclables pour chaque département
query1 <- opq(bbox = st_bbox(departements_metro[i,])) %>%
  add_osm_feature(key = "highway", value = "cycleway")
cycleway1 <- osmdata_sf(query1)$osm_lines
cycleway1 <- cycleway1 %>% 
  select(geometry)

query2 <- opq(bbox = st_bbox(departements_metro[i,])) %>%
  add_osm_feature(key = "bicycle", value = "designated")
cycleway2 <- osmdata_sf(query2)$osm_lines
cycleway2 <- cycleway2 %>% 
  select(geometry)

query3 <- opq(bbox = st_bbox(departements_metro[i,])) %>%
  add_osm_feature(key = "cycleway", value = "lane")
cycleway3 <- osmdata_sf(query3)$osm_lines
cycleway3 <- cycleway3 %>% 
  select(geometry)

all_cycleways <- bind_rows(cycleway1, cycleway2, cycleway3)
mapview(all_cycleways)

all_cycleways <- st_intersection(departements[i,], all_cycleways)
all_cycleways <- tibble(all_cycleways)

all_all_cycleways <- rbind(all_cycleways, all_all_cycleways)
break
}

# mapview(st_as_sf(all_all_cycleways))

## Calcul de la distance la plus courte entre le centre de la commune et la piste cyclable la plus proche

Nous calculons ici la distance entre le centroïd de la commune et la piste cyclable la plus proche. 

# ```{r, eval=F}

# Packages
pacman::p_load(sf,tidyverse,tmap)

# Chargement et traitements des couches
setwd("~/OT/TRANSFOOD/DATA/")
communes <- st_read("C:/Users/otheureaux/Documents/OT/DECOUPAGES_FR/COMMUNE.shp")
# mapview(communes)

### Calcul du centroïd de chaque commune
communes$geom_centroid <- st_centroid(st_make_valid(communes))
communes_centroid <- communes %>% 
  select(INSEE_COM, INSEE_DEP, geom_centroid)

communes_centroid_67 <- communes_centroid %>% 
  filter(INSEE_DEP == '67')

# glimpse(communes_centroid_67)

all_all_cycleways_sf <- all_all_cycleways %>% 
  st_as_sf() %>% 
  st_transform(2154)

# Calculer les distances géodésiques entre chaque logement et chaque commerce
distances <- st_distance(communes_centroid_67$geom_centroid, all_all_cycleways_sf) # l'unité est le mètre

################# J'ai comparé st_distance avec distm
# distances <- st_distance(carreau_centroid_region, commerces_region)
# J'ai testé les 2 méthodes, qui renvoient le même résultat
##########################################################################

# Trouver le commerce le plus proche pour chaque logement
plus_proches_PC <- data.frame(apply(distances, MARGIN = 1, FUN = min))
colnames(plus_proches_PC) <- c("distance")
communes_dist <- bind_cols(all_all_cycleways_sf, plus_proches_PC)




